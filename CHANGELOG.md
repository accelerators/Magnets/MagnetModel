# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.1.0] - 23-06-2020

* Bug related to calibration fixed by Gael in his model

# [1.0.0] - 30-10-2019

* First official release before ESRF re-start with EBS storage ring
