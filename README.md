# MagnetModel

Library in charge of magnetic strenghs to coil currents calculation. This library implements computation in the two directions (from currents to magnetic coeffs and from magnetic coeffs to currents). Thank's to **Gael Lebec** (from ASD) who is the author of the sextupole model.

## Cloning

To clone this project, simply type:

```
git clone git@gitlab.esrf.fr:accelerators/Magnets/MagnetModel.git
```

## Building and Installation

### Dependencies

The project has the following dependencies.

#### Project Dependencies 

* [Eigen](http://eigen.tuxfamily.org/index.php?title=Main_Page)

#### Toolchain Dependencies

* C++11 compliant compiler.
* CMake 3.0 or greater is required to perform the build. 


### Build library

Instructions on building the project.

CMake example:

```bash
cd MagnetModel/lib
mkdir -p build/<os>
cd build/<os>
cmake ../.. -DCMAKE_INSTALL_PREFIX=<folder where lib must be installed>
make
```

### Installation

In the same folder than above

```bash
make install
```
The include files and the library will be installed in the folder given during the cmake command

## Tests

To build the test software:
* Adapt the **INSTALL_DIR** variable in the CMakeLists.txt file to the folder you have used as installation folder.

```bash
cd SextuCorrModels/test
mkdir -p build/<os>
cd build/<os>
cmake ../..
make
```
To run the test software (in the same folder than above)

```bash
./TestSextuCorr <Path to the Parameter files folder>
```
