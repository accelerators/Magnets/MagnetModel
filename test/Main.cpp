// MagnetCorrModels.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include <chrono>
#include <ctime>
#include "Sextupole.h"

using namespace MagnetModel;

// Basic test functions
Sextupole model_init(std::string path, std::string strengths_name, std::string param_name, std::string mag_name);
void model_check(Sextupole& sextu, double i_main, double i_ch1, double i_ch2, double i_ch3, double i_ch4);
void model_multi_from_currents(Sextupole& sextu, double i_main, double i_ch1, double i_ch2, double i_ch3, double i_ch4);
void model_currents_from_multi(Sextupole& sextu, double a_1, double b_1, double a_2, double b_3, std::string l_or_nl);

//-----------------------------------------------------
int main(int argc,char *argv[])
{
	if (argc != 2) {
		std::cerr << "TestSextuCorr usage: TestSextuCorr <Path to parameter files folder>" << std::endl;
		exit(-1);
	}
	std::string path(argv[1]);
	if (path[path.size() - 1] != '/') {
		path.append(1,'/');
	}
	
	// --- Initialize
	// Magnet file names
	std::string meas_strength_filename = "SD1_meas_strengths.dat";
	std::string param_filename = "SD1_params.dat";
	// Build the model
	Sextupole sextu = model_init(path, meas_strength_filename, param_filename, "SD1_TEST");
	// --- Check the model
	// Initial values for the currents
	double i_main = 92;
	double i_ch1 = -0.1557478, i_ch2 = 0.91462704, i_ch3 = 0.166953941, i_ch4 = 0.043270705;
	model_check(sextu, i_main, i_ch1, i_ch2, i_ch3, i_ch4);
	// Main loop
	std::string str_in;
	double a_1, b_1, a_2, b_3;
	while (true) {
		std::cout << "Commands: \"q\" to quit, \"c\" to enter currents, \"m\" to enter multipoles\n";
		std::cin >> str_in;
		if (!str_in.compare("q") ) 
			break;
		else if ( !str_in.compare("c") ) {
			std::cout << "I_main [A]: "; std::cin >> str_in; i_main = stod(str_in);
			std::cout << "I_channel_1 [A]: "; std::cin >> str_in; i_ch1 = stod(str_in);
			std::cout << "I_channel_2 [A]: "; std::cin >> str_in; i_ch2 = stod(str_in);
			std::cout << "I_channel_3 [A]: "; std::cin >> str_in; i_ch3 = stod(str_in);
			std::cout << "I_channel_4 [A]: "; std::cin >> str_in; i_ch4 = stod(str_in);
			model_multi_from_currents(sextu, i_main, i_ch1, i_ch2, i_ch3, i_ch4);
		}
		else if ( !str_in.compare("m") ) {
			std::cout << "a_1 [Tmm]: "; std::cin >> str_in; a_1 = stod(str_in);
			std::cout << "b_1 [Tmm]: "; std::cin >> str_in; b_1 = stod(str_in);
			std::cout << "a_2 [T]: "; std::cin >> str_in; a_2 = stod(str_in);
			std::cout << "b_3 [T/mm]: "; std::cin >> str_in; b_3 = stod(str_in);
			model_currents_from_multi(sextu, a_1, b_1, a_2, b_3, "l"); // Linear solver
			model_currents_from_multi(sextu, a_1, b_1, a_2, b_3, "nl"); // NL solver
		}
	}

    return 0;
}

//-----------------------------------------------------
// Test function
// Initialize a sextupole model using the specified files
Sextupole model_init(std::string path, std::string meas_strength_filename, std::string param_filename, std::string mag_name) {

	std::chrono::time_point<std::chrono::system_clock> t0, t1;
	t0 = std::chrono::system_clock::now();

	// Initialize a Sextupole 
	Sextupole sextu;

	// Initialise the magnet
  try {
	  sextu.init(path + meas_strength_filename, path + param_filename, mag_name);
  } catch (std::exception& e) {
    std::cerr << "Fatal error: " << e.what() << std::endl;
    std::exit(-1);
  }
	t1 = std::chrono::system_clock::now();

	std::cout << "-----------------------------------------\n";
	std::cout << "Model built in " << std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count() << " us\n";

	// Return the model
	return sextu;
}

//-----------------------------------------------------
// Test function
// Compute the multipoles from the specified current, then the currents from the computed multipoles
void model_check(Sextupole& sextu, double i_main, double i_ch1, double i_ch2, double i_ch3, double i_ch4) {

	std::chrono::time_point<std::chrono::system_clock> t0, t1, t2, t3, t4, t5, t6;
	t0 = std::chrono::system_clock::now();

	// Corrector current vector
	std::vector<double> i_corr = { i_ch1, i_ch2, i_ch3, i_ch4 };
	// Set the currents
	t1 = std::chrono::system_clock::now();
	sextu.set_current_main(i_main);
	sextu.set_current_corr(i_corr);

	// Compute and set the multipoles
	sextu.set_multipoles_from_currents(i_main, i_corr);
	t2 = std::chrono::system_clock::now();

	// Extract the multipoles
	Multipole multi;
	multi = sextu.get_multipoles();
	std::vector<double> an = multi.get_an();
	std::vector<double> bn = multi.get_bn();

	// Inverse (linear solver)
	t3 = std::chrono::system_clock::now();
	sextu.solve_linear(an[0], bn[0], an[1], bn[2]);
	t4 = std::chrono::system_clock::now();
	double curr_main_lin;
	std::vector<double> curr_corr_lin = { 0, 0, 0, 0 };
	sextu.get_solver_results(curr_main_lin, curr_corr_lin);
	// Check the results
	sextu.set_multipoles_from_currents(curr_main_lin, curr_corr_lin);
	Multipole multi_lin;
	multi_lin = sextu.get_multipoles();
	std::vector<double> an_lin = multi_lin.get_an();
	std::vector<double> bn_lin = multi_lin.get_bn();

	// Inverse (NL solver)
	double eps = 1e-9; // Precision
	t5 = std::chrono::system_clock::now();
	sextu.solve_newton(an[0], bn[0], an[1], bn[2]);
	t6 = std::chrono::system_clock::now();
	double curr_main_nl;
	std::vector<double> curr_corr_nl = { 0, 0, 0, 0 };
	sextu.get_solver_results(curr_main_nl, curr_corr_nl);
	// Check the results
	sextu.set_multipoles_from_currents(curr_main_nl, curr_corr_nl);
	Multipole multi_nl;
	multi_nl = sextu.get_multipoles();
	std::vector<double> an_nl = multi_nl.get_an();
	std::vector<double> bn_nl = multi_nl.get_bn();

	std::cout << "-----------------------------------------\n";
	std::cout << "CURRENTS\n";
	std::cout << "Main current: " << i_main << " A\n";
	std::cout << "Corrector current: " << i_corr[0] << " A, " << i_corr[1] << " A, " << i_corr[2] << " A, " << i_corr[3] << " A\n";
	std::cout << '\n';
	std::cout << "MULTIPOLES FROM CURRENTS\n";
	std::cout << "a1: " << an[0] << " Tmm, b1: " << bn[0] << " Tmm\n";
	std::cout << "a2: " << an[1] << " T, b2: " << bn[1] << " T\n";
	std::cout << "a3: " << an[2] << " T/mm, b3: " << bn[2] << " T/mm\n";
	std::cout << '\n';
	std::cout << "Multipoles computated in " << std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count() << " us\n";
	std::cout << "-----------------------------------------\n";
	std::cout << "LINEAR SOLVER\n";
	std::cout << "Main current: " << curr_main_lin << " A\n";
	std::cout << "Corrector current: " << curr_corr_lin[0] << " A, " << curr_corr_lin[1] << " A, " << curr_corr_lin[2] << " A, " << curr_corr_lin[3] << " A\n";
	std::cout << '\n';
	std::cout << "Currents computed in " << std::chrono::duration_cast<std::chrono::microseconds>(t4 - t3).count() << " us\n";
	std::cout << "MULTIPOLES FROM CURRENTS / LINEAR SOLVER\n";
	std::cout << "a1: " << an_lin[0] << " Tmm, b1: " << bn_lin[0] << " Tmm\n";
	std::cout << "a2: " << an_lin[1] << " T, b2: " << bn_lin[1] << " T\n";
	std::cout << "a3: " << an_lin[2] << " T/mm, b3: " << bn_lin[2] << " T/mm\n";
	std::cout << '\n';
	std::cout << "-----------------------------------------\n";
	std::cout << "NON-LINEAR SOLVER\n";
	std::cout << "Main current: " << curr_main_nl << " A\n";
	std::cout << "Corrector current: " << curr_corr_nl[0] << " A, " << curr_corr_nl[1] << " A, " << curr_corr_nl[2] << " A, " << curr_corr_nl[3] << " A\n";
	std::cout << '\n';
	std::cout << "Converged in " << sextu.get_iterations() << " iterations\n";
	std::cout << "Currents computed in " << std::chrono::duration_cast<std::chrono::microseconds>(t6 - t5).count() << " us\n";
	std::cout << "MULTIPOLES FROM CURRENTS / NL SOLVER\n";
	std::cout << "a1: " << an_nl[0] << " Tmm, b1: " << bn_nl[0] << " Tmm\n";
	std::cout << "a2: " << an_nl[1] << " T, b2: " << bn_nl[1] << " T\n";
	std::cout << "a3: " << an_nl[2] << " T/mm, b3: " << bn_nl[2] << " T/mm\n";
	std::cout << '\n';
}

//-----------------------------------------------------
// Test function
// Compute the multipoles from the specified currents
void model_multi_from_currents(Sextupole& sextu, double i_main, double i_ch1, double i_ch2, double i_ch3, double i_ch4) {
	
	// Set the currents
	std::vector<double> i_corr = { i_ch1, i_ch2, i_ch3, i_ch4 };
	sextu.set_current_main(i_main);
	sextu.set_current_corr(i_corr);

	// Compute and set the multipoles
	sextu.set_multipoles_from_currents(i_main, i_corr);

	// Extract the multipoles
	Multipole multi = sextu.get_multipoles();
	std::vector<double> an = multi.get_an();
	std::vector<double> bn = multi.get_bn();

	std::cout << "-----------------------------------------\n";
	std::cout << "MULTIPOLES FROM CURRENTS\n";
	std::cout << "a1: " << an[0] << " Tmm, b1: " << bn[0] << " Tmm\n";
	std::cout << "a2: " << an[1] << " T, b2: " << bn[1] << " T\n";
	std::cout << "a3: " << an[2] << " T/mm, b3: " << bn[2] << " T/mm\n";
	std::cout << '\n';
	
}

//-----------------------------------------------------
// Test function
// Compute the currents from the multipoles
void model_currents_from_multi(Sextupole& sextu, double a_1, double b_1, double a_2, double b_3, std::string l_or_nl) {

	
	// Define the multipoles and change the strengths in a multipole object
	std::vector<double> an = { a_1, a_2, 0 };
	std::vector<double> bn = { b_1, 0, b_3 };
	Multipole multi = sextu.get_multipoles(); // By doing so, we get the a_n, b_n and the reference radius
	multi.set_an_bn(an, bn);

	// Set the solver
	if (!l_or_nl.compare("l")) sextu.set_solver(0); // Linear solver
	else sextu.set_solver(1); // Newton's method (default value)

	// Solve
	sextu.set_currents_from_multipoles(multi);

	// Get the computed currents
	// Get the results
	double curr_main;
	std::vector<double> curr_corr = { 0, 0, 0, 0 };
	sextu.get_solver_results(curr_main, curr_corr);
	
	// The linear and the NL solver can also be called directly, as shown below
	/* 
	// Solve
	if (l_or_nl.compare("l"))
		sextu.solve_linear(a_1, b_1, a_2, b_3);
	else sextu.solve_newton(a_1, b_1, a_2, b_3);
	// Get the results
	double curr_main;
	std::vector<double> curr_corr = { 0, 0, 0, 0 };
	sextu.get_solver_results(curr_main, curr_corr);
	// Set the currents and compute the multipoles
	sextu.set_multipoles_from_currents(curr_main, curr_corr);
	// Get the results
	Multipole multi_lin = sextu.get_multipoles();
	an = multi_lin.get_an();
	bn = multi_lin.get_bn();
	*/

	std::cout << "-----------------------------------------\n";
	std::cout << "SOLVER: "; if (!l_or_nl.compare("l")) std::cout << "Linear\n"; else std::cout << "Newton's method\n";
	std::cout << "Main current: " << curr_main << " A\n";
	std::cout << "Corrector current: " << curr_corr[0] << " A, " << curr_corr[1] << " A, " << curr_corr[2] << " A, " << curr_corr[3] << " A\n";
	std::cout << '\n';
	std::cout << "MULTIPOLES FROM CURRENTS\n";
	std::cout << "a1: " << an[0] << " Tmm, b1: " << bn[0] << " Tmm\n";
	std::cout << "a2: " << an[1] << " T, b2: " << bn[1] << " T\n";
	std::cout << "a3: " << an[2] << " T/mm, b3: " << bn[2] << " T/mm\n";
	std::cout << '\n';

}
