cmake_minimum_required(VERSION 2.8)

project(app_project)

set(INSTALL_DIR /segfs/tango/contrib/debian9/MagnetModel)

set(INCL_USER ${INSTALL_DIR}/include)
set(INCL_EIGEN /segfs/tango/contrib/eigen/include/eigen3)
include_directories(${INCL_USER} ${INCL_EIGEN})

set(LIB_USER ${INSTALL_DIR}/lib)
link_directories(${LIB_USER})

add_executable(TestSextuCorr Main.cpp)

target_link_libraries(TestSextuCorr MagnetModel)

