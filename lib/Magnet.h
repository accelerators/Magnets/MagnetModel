#pragma once

#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include "Multipole.h"
#include "Interpolation.h"
#include <cmath>

namespace MagnetModel {

class Magnet 
{
public:
	// --- Constructors
	Magnet();
	Magnet(double curr_main, int n_corr, int n_multi);
	Magnet(double curr_main, int n_corr, int n_multi, double r);
	// --- Set and get attributes
	// Main current
	int set_current_main(double curr_main);
	double get_current_main();
	// Corrector currents
	int set_current_corr(const std::vector<double>& curr_corr);
	std::vector<double> get_current_corr();
	int get_current_corr(std::vector<double>& curr_corr);
	size_t get_corrector_number();
	// Magnet type
	int set_magnet_type(std::string m_type_str);
	std::string get_magnet_type();
	// Multipoles
	int set_multipoles(const Multipole& multi);
	Multipole get_multipoles();
	int get_multipoles(Multipole&  multi);
	int set_multipole_strengths(const std::vector<double>& a, const std::vector<double>& b);
	// Scale
	int set_scale(double s);
	double get_scale();
	// Offset
	int set_offset(double o);
	double get_offset();
	// pseudo current offset (for DQ mdoel)
	double get_pseudo_current_offset();
	// Laod a matrix from a file
	int load_matrix(std::string file_name,int row,int col,Eigen::MatrixXd &m);
	// Load magnet parameters from file
	int set_params_from_file(std::string file_name, std::string mag_s_n);
	// Load excitation curve
	int set_excitation_from_file(std::string file_name);
	// Set excitation curve
	int set_excitation_curve(Interpolation ex_curve);
	// Get excitation curve
	Interpolation get_excitation_curve();
	// Read excitation value
	double get_excitation();
	// Read excitation value
	double get_excitation(double curr_main);
	// Read excitation value
	double get_excitation_linear();
	// Read excitation value
	double get_excitation_linear(double curr_main);
	// Solve the current for the given strength
	double solve_main_strength(double target_strength);
	// Solve the current for the given strength
	double solve_main_strength_linear(double target_strength);
	// Set the tolerance of the solver
	int set_solver_tolerance(double eps);
	// Get the solver tolerance
	double get_solver_tolerance();
	// Set the maximum number of iterations
	int set_solver_max_iterations(int max_iter);
	// Get the maximum number of iterations
	int get_solver_max_iterations();
	// Return true if model has a main current
	virtual bool has_main_current() { return true; };
	// Return strength names (main,skew,h,v)
	virtual void get_strength_names(std::vector<std::string>& names) {};
	// Return strength unit (rad,m-1,m-2,m-3)
	virtual void get_strength_units(std::vector<std::string>& units) {};
	// Compute strength(s) from current(s) in standard unit
	virtual void compute_strengths(double magnet_rigidity_inv,std::vector<double>& in_currents,std::vector<double>& out_strengths) {};
	// Compute current(s) from strength(s)
	virtual void compute_currents(double magnet_rigidity,std::vector<double>& in_strengths,std::vector<double>& out_currents) {};
  // Compute pseudo currents from currents (SH Only)
  virtual void compute_pseudo_currents_from_currents(std::vector<double>& in_currents,std::vector<double>& out_currents) {};
  // Compute strength from pseudo currents (SH Only, idx=0 for H,idx=0 for V,idx=0 for SQ,idx=3 for SEXT)
  virtual void compute_strength_from_pseudo(double magnet_rigidity_inv,int idx,double& in_current,double& out_strength) {};

private:
	std::string serial_number;		// Magnet serial number
	std::string magnet_type;		// Magnet type
	double current_main;			// Main magnet current	
	std::vector<double> current_corr;	// Corrector currents (the number of channels depends on the magnet type)
	Multipole multipole;			// Field multipoles, used for strength computations
	double scale;				// Scale to be applied to the excitation curve (calibration)
	double offset;				// field offset (hysteresis)
	double pseudo_current_offset;		// For DQ model
	Interpolation excitation_curve;		// Excitation curve
	Interpolation rexcitation_curve;	// Inverse Excitation curve
	double solver_tolerance;
	int solver_max_iter;
protected:
	void split(std::vector<std::string> &tokens, const std::string &text, char sep);
};

} // end namespace MagnetModel
