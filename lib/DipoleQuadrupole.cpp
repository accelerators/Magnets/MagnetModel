#include "DipoleQuadrupole.h"

namespace MagnetModel {

//-----------------------------------------------------
// Init a DQ
int DipoleQuadrupole::init(bool focusing, double scale, std::string strength_file_name, std::string param_file_name,
           std::string dipole_strength_file_name,std::string matrix_file_name, std::string mag_s_n) {

  // Magnet type
  set_magnet_type("dipolequadrupole");
  this->focusing = focusing;

  // Initialize scales, offsets, etc.
  set_params_from_file(param_file_name, mag_s_n);

  // Initialize the excitation curve
  set_scale( scale*get_scale() );
  set_excitation_from_file(strength_file_name);

  // Initialise the dipole excitation curve
  dipole.set_excitation_from_file(dipole_strength_file_name);

  // Load the matrix
  load_matrix(matrix_file_name,2,2,M);

  // Compute pseudo inverse
  Mp = M.completeOrthogonalDecomposition().pseudoInverse();

  return 0;

}


bool DipoleQuadrupole::has_main_current() {
  return true;
}

void DipoleQuadrupole::get_strength_names(std::vector<std::string>& names) {
  // The following names are used to create Tango attributes
  names.clear();
  names.push_back("Strength");
  names.push_back("Strength_H");
}

void DipoleQuadrupole::get_strength_units(std::vector<std::string>& units) {
  // The following units are used to create Tango attributes properties
  units.clear();
  units.push_back("m-1");
  units.push_back("rad");
}

//-----------------------------------------------------
// Compute strength(s) from current(s) in standard unit
void DipoleQuadrupole::compute_strengths(double magnet_rigidity_inv,std::vector<double>& in_currents,std::vector<double>& out_strengths) {

  if (in_currents.size() != 2)
    throw std::invalid_argument(get_magnet_type()+"::compute_strengths() 2 currents expected");

  // Compute pseudo currents
  auto mapped_currents = Eigen::Map<Eigen::VectorXd>(in_currents.data(), in_currents.size());
  Eigen::VectorXd pI = M * mapped_currents;
  double dpI = pI(1) - get_pseudo_current_offset();
  double dSign = (dpI<0.0)?-1.0:1.0;

  // Compute strengths
  out_strengths.resize(2);
  out_strengths[1] = -dSign*dipole.get_excitation_linear(fabs(dpI)) * magnet_rigidity_inv;
  if(focusing) {
    out_strengths[0] = get_excitation_linear(pI(0)) * magnet_rigidity_inv;
  } else {
    out_strengths[0] = -get_excitation_linear(pI(0)) * magnet_rigidity_inv;
  }

}

//-----------------------------------------------------
// Compute current(s) from strength(s)
void DipoleQuadrupole::compute_currents(double magnet_rigidity,std::vector<double>& in_strengths,std::vector<double>& out_currents) {

  if (in_strengths.size() != 2)
    throw std::invalid_argument(get_magnet_type()+"::compute_currents() 2 strengths expected");

  // Compute pseudo currents
  Eigen::Vector2d pI;
  double dStrength = in_strengths[1] * magnet_rigidity;
  double dSign = (dStrength<0.0)?-1.0:1.0;
  pI(1) = -dSign*dipole.solve_main_strength_linear(fabs(dStrength)) + get_pseudo_current_offset();

  if(focusing) {
    pI(0) = solve_main_strength_linear(in_strengths[0] * magnet_rigidity);
  } else {
    pI(0) = solve_main_strength_linear(-in_strengths[0] * magnet_rigidity);
  }

  Eigen::VectorXd I = Mp * pI;
  out_currents.resize(2);
  Eigen::Map<Eigen::VectorXd>(out_currents.data(),out_currents.size()) = I;

}

} // end namespace MagnetModel