#include "Interpolation.h"
#include <algorithm>
#include "math.h"
#include <iostream>

namespace MagnetModel {

//-----------------------------------------------------
// Constructor
Interpolation::Interpolation() {
	points.clear();
	linear_coefs.clear();
	spline_coefs.clear();
}

//-----------------------------------------------------
// Constructor
Interpolation::Interpolation(std::vector<double> x, std::vector<double> y) {
	
	// Set the points, etc.
	set_points(x, y);
}

//-----------------------------------------------------
// Set and sort the points
// Pre-compute the slopes for linear interpolation
int Interpolation::set_points(std::vector<double> x, std::vector<double> y) {
	
	// Clear previous points
	points.clear();

	// Build the set of points
	int n = x.size();
	for (int k = 0; k < n; k++) {
		std::vector<double> point_k;
		point_k.push_back(x[k]);
		point_k.push_back(y[k]);
		points.push_back(point_k);
	}
	
	
	/*std::cout <<"===============\n";
	double x0 = points[0][0];
	double y0 = points[0][1];	
	std::cout<<"x: "<< x0 << ", y:"<< y0 <<"\n";*/

	// Sort the points in increasing x values
	sort_points(points);

	/*std::cout <<"------------\n";
	x0 = points[0][0];
	y0 = points[0][1];	
	std::cout<<"x: "<< x0 << ", y:"<< y0 <<"\n";*/

	// Compute the slopes
	set_linear_coefs();

	// Compute spline coefficients
	set_spline_coefs();
	
	return 0;
}

//-----------------------------------------------------
// Initialize the coefficients (i.e. slopes) for the linear interpolations
int Interpolation::set_linear_coefs() {

	// Initialize
	int n = points.size() - 1;
	linear_coefs.clear();

	// Compute the slopes
	for(int i = 0; i < n; i++)
		linear_coefs.push_back((points[i + 1][1] - points[i][1]) / (points[i + 1][0] - points[i][0]));

	return 0;
}

//-----------------------------------------------------
// Initialize the coefficients for spline interpolation
// Adapted from https://kluge.in-chemnitz.de/opensource/spline/
int Interpolation::set_spline_coefs() {

	// Initialize
	int n = points.size();
	Eigen::MatrixXd m = Eigen::MatrixXd::Zero(n, n);
	Eigen::MatrixXd u(n, 1);
	
	// Fill the matrix and vectors
	for (int i = 1; i < n - 1; i++) {
		// Tri-diagonal matrix
		m(i, i - 1) = 1. / 3. * (points[i][0] - points[i - 1][0]);
		m(i, i) = 2. / 3. * (points[i + 1][0] - points[i - 1][0]);
		m(i, i + 1) = 1. / 3. * (points[i + 1][0] - points[i][0]);
		// Vector
		u(i) = (points[i + 1][1] - points[i][1]) / (points[i + 1][0] - points[i][0])
					- (points[i][1] - points[i - 1][1]) / (points[i][0] - points[i - 1][0]);
	}
	// Boundary conditions
	m(0, 0) = 2.;
	m(0, 1) = 0;
	m(n - 1, n - 1) = 2.;
	m(n - 1, n - 2) = 0;
	u(0) = 0;
	u(n - 1) = 0;

	// Compute the quadratic coefficients from the inverse of m
	Eigen::VectorXd b = m.colPivHouseholderQr().solve(u);
	
	// Linear coefficients
	Eigen::VectorXd c(n);
	for (int i = 0; i < n - 1; i++)
		c(i) = (points[i + 1][1] - points[i][1]) / (points[i + 1][0] - points[i][0]) - 1. / 3. * (2. * b(i) + b(i + 1)) * (points[i + 1][0] - points[i][0]);

	// Cubic coefficients
	Eigen::VectorXd a(n);
	for (int i = 0; i < n - 1; i++)
		a(i) = 1. / 3. * (b(i + 1) - b(i)) / (points[i + 1][0] - points[i][0]);

	// Compute the last coefficients from boundary conditions
	double h = points[n - 1][0] - points[n - 2][0];
	a(n - 1) = 0;
	c(n - 1) = 3. * a(n - 2) * h * h + 2. * b(n - 2) * h + c(n - 2);

	// Set the spline coefficients
	spline_coefs.clear();
	for (int i = 0; i < n - 1; i++) {
		std::vector<double> coef_i;
		coef_i.push_back(a(i));
		coef_i.push_back(b(i));
		coef_i.push_back(c(i));
		spline_coefs.push_back(coef_i);
	}

	return 0;
}

//-----------------------------------------------------
int Interpolation::find_interval(double x) {

	// Find the interval [x_i, x_i+1]
	// binary search, right most item.
	int R = points.size();
	int L = 0;
	int m;

	while(L<R) {
		m = (L + R) / 2;
		if( x < points[m][0] ) {
			R = m;
		} else {
			L = m + 1;
		}
	}
	
	return (R<=0)?0:(R-1);

}

//-----------------------------------------------------
// Linear interpolation at x
double Interpolation::linear(double x) {

	// Find the interval [x_i, x_i+1]
	int i = find_interval(x);

	// Linear interpolation
  if(i==points.size()-1)
    // Linear extrapolation at the end
    i--;

  return (x - points[i][0]) * linear_coefs[i] + points[i][1];

}

//-----------------------------------------------------
// Derivative of the linear interpolation at x
double Interpolation::linear_derivative(double x) {

	// Find the interval [x_i, x_i+1]
	int i = find_interval(x);

  if(i==points.size()-1)
    // Linear extrapolation at the end
    i--;

	// Return the slope
	return linear_coefs[i];

}

//-----------------------------------------------------
// Spline interpolation at x
double Interpolation::spline(double x) {

	// Find the interval [x_i, x_i+1]
	int n = points.size() - 1;
	int i = find_interval(x);

	// Spline interpolation
	double y;
	if (x < points[0][0])
		// Linear interpolation if x < x_0
		y = (x - points[0][0]) * linear_coefs[0] + points[0][1];
	else if (x < points[n][0])	
		// Spline interpolation
		y = spline_coefs[i][0] * (x - points[i][0]) * (x - points[i][0]) * (x - points[i][0]) + spline_coefs[i][1] * (x - points[i][0]) * (x - points[i][0])
				+ spline_coefs[i][2] * (x - points[i][0]) + points[i][1];
	else 
		// Linear interpolation if x > x_N-1
		y = (x - points[n][0]) * linear_coefs[n-1] + points[n][1];

	return y;
}

//-----------------------------------------------------
// Derivative ofthe spline interpolation at x
double Interpolation::spline_derivative(double x) {

	// Find the interval [x_i, x_i+1]
	int n = points.size() - 1;
	int i = find_interval(x);

	// Derivative of the spline interpolation
	double dy_dx;
	if (x < points[0][0])
		// Linear interpolation if x < x_0
		dy_dx = linear_coefs[0];
	else if (x < points[n][0])
		// Spline interpolation
		dy_dx = 3 * spline_coefs[i][0] * (x - points[i][0]) * (x - points[i][0]) + 2 * spline_coefs[i][1] * (x - points[i][0]) + spline_coefs[i][2];
	else
		// Linear interpolation if x > x_N-1
		dy_dx = linear_coefs[n - 1];

	return dy_dx;
}

//-----------------------------------------------------
// Sort the points in increasing x order
void Interpolation::sort_points(std::vector<std::vector<double>> & v) {

	// Define a lamba for comparing the points
	auto comp = [](std::vector<double> xy_1, std::vector<double> xy_2) { return (xy_1[0] < xy_2[0]); };

	// Sort increasing x values
	std::stable_sort(v.begin(), v.end(), comp);	
	
}

//-----------------------------------------------------
// Return inverse curve
Interpolation Interpolation::inverse() {
  std::vector<double> x;
  std::vector<double> y;
  for (size_t k = 0; k < points.size(); k++) {
    x.emplace_back(points[k][0]);
    y.emplace_back(points[k][1]);
  }
  return Interpolation(y, x);
}


//-----------------------------------------------------
// Constructor
NInterpolation::NInterpolation() {
	x_values.clear();
	y_values.clear();
	n_interpolation.clear();
	r = 0;
	c = 0;
}

//-----------------------------------------------------
// Constructor
NInterpolation::NInterpolation(std::vector<double> x, std::vector<Eigen::MatrixXd> y) {

	set_points(x, y);
}

//-----------------------------------------------------
// Initialize a set of interpolation functions
int NInterpolation::set_points(std::vector<double> x, std::vector<Eigen::MatrixXd> y) {
	
	// Initialize x values
	x_values = x;

	// Initialize y values
	y_values = y;

	// Initialize interpolation functions
	n_interpolation.clear();
	c = y[0].cols();
	r = y[0].rows();
	int n = x_values.size();
	// For each function
	for (int i = 0; i < r; i++) 
		for (int j = 0; j < c; j++) {
			// Initialize a vector
			std::vector<double> v;
			v.clear();
			// Get the values at each current
			for (int k = 0; k < n; k++)
				v.push_back(y[k](i, j));
			// Build and store the interpolation function
			Interpolation interp(x, v);
			n_interpolation.push_back(interp);
		}
	
	return 0;
}

//-----------------------------------------------------
// Spline interpolation at x
Eigen::MatrixXd NInterpolation::spline(double x) {

	// Initialize
	Eigen::MatrixXd m;
	m.resize(r, c);
	
	// Spline interpolation
	spline(x, m);

	// Return the N dimensional value at x
	return m;
}

//-----------------------------------------------------
// Spline interpolation at x
int NInterpolation::spline(double x, Eigen::MatrixXd& m) {

	// Check that m is compatible
	if ((m.rows() != r) || (m.cols() != c)) return -1;

	// Spline interpolation
	int k = 0;
	for (int i = 0; i < r; i++)
		for (int j = 0; j < c; j++) {
			m(i, j) = n_interpolation[k].spline(x);
			k++;
		}

	return 0;
}

//-----------------------------------------------------
// Linear interpolation at x
Eigen::MatrixXd NInterpolation::linear(double x) {

	// Initialize
	Eigen::MatrixXd m;
	m.resize(r, c);

	// Spline interpolation
	linear(x, m);

	// Return the N dimensional value at x
	return m;
}

//-----------------------------------------------------
// Linear interpolation at x
int NInterpolation::linear(double x, Eigen::MatrixXd& m) {

	// Check that m is compatible
	if ((m.rows() != r) || (m.cols() != c)) return -1;

	// Spline interpolation
	int k = 0;
	for (int i = 0; i < r; i++)
		for (int j = 0; j < c; j++) {
			m(i, j) = n_interpolation[k].linear(x);
			k++;
		}

	return 0;
}

} // end namespace MagnetModel
