#pragma once

#include <vector>

namespace MagnetModel {

class Multipole
{
public:
	// --- Constructors
	Multipole();
	Multipole(int n);
	Multipole(int n, double r);
	// --- Get and set the reference radius
	double get_radius();
	int set_radius(double r);
	// --- Get and set the multipoles
	std::vector<double> get_an();
	std::vector<double> get_bn();
	int get_an(std::vector<double>& a);
	int get_bn(std::vector<double>& b);
	int get_an_bn(std::vector<double>& a, std::vector<double>& b);
	int set_an(const std::vector<double>& a);
	int set_bn(const std::vector<double>& b);
	int set_an_bn(const std::vector<double>& a, const std::vector<double>& b);

private:
	double radius;			// Reference radius [mm]
	std::vector<double> an;	// Skew multipole values at reference radius [Tmm]
	std::vector<double> bn; // Normal multipole values at reference radius [Tmm]
};

} // end namespace MagnetModel