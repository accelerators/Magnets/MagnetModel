#pragma once

#include "Magnet.h"

namespace MagnetModel {

class SH5Magnet : public Magnet {

public:
// Constructor and initialization
  SH5Magnet() : Magnet(0, 5, 0, 0) {}

  int init(double scale,std::string s_strength_file_name, std::string h_strength_file_name, std::string v_strength_file_name,
           std::string sq_strength_file_name,std::string matrix_file_name);

  // Implementation of virtual methods (described in Magnet.h)
  bool has_main_current();
  void get_strength_names(std::vector<std::string> &names);
  void get_strength_units(std::vector<std::string> &units);
  void compute_strengths(double magnet_rigidity_inv, std::vector<double> &in_currents, std::vector<double> &out_strengths);
  void compute_currents(double magnet_rigidity, std::vector<double> &in_strengths, std::vector<double> &out_currents);
  void compute_pseudo_currents_from_currents(std::vector<double>& in_currents,std::vector<double>& out_currents);
  void compute_currents_from_pseudo_currents(std::vector<double>& in_currents,std::vector<double>& out_currents);
  void compute_strength_from_pseudo(double magnet_rigidity_inv,int idx,double& in_current,double& out_strength);

private:

  bool focusing;
  Magnet h;
  Magnet v;
  Magnet sq;
  Eigen::MatrixXd M;  // Pseudo current matrix
  Eigen::MatrixXd Mp; // Pseudo current inverse matrix

};

} // end namespace MagnetModel