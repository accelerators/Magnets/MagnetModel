#include "SH5Magnet.h"

namespace MagnetModel {

//-----------------------------------------------------
// Init a SH magnet (5 correctors for setxu, h,v and skew quad)
int SH5Magnet::init(double scale,std::string s_strength_file_name, std::string h_strength_file_name,
                    std::string v_strength_file_name, std::string sq_strength_file_name,std::string matrix_file_name) {

  // Magnet type
  set_magnet_type("sh5magnet");

  // Set current and initialise sextu excitation curve
  set_scale(scale);
  set_excitation_from_file(s_strength_file_name);

  // Initialise the H excitation curve
  h.set_scale(scale);
  h.set_excitation_from_file(h_strength_file_name);

  // Initialise the V excitation curve
  v.set_scale(scale);
  v.set_excitation_from_file(v_strength_file_name);

  // Initialise the skew quad excitation curve
  sq.set_scale(scale);
  sq.set_excitation_from_file(sq_strength_file_name);

  // Load the matrix
  load_matrix(matrix_file_name,4,5,M);

  // Compute pseudo inverse
  Mp = M.completeOrthogonalDecomposition().pseudoInverse();

  return 0;

}


bool SH5Magnet::has_main_current() {
  return false;
}

void SH5Magnet::get_strength_names(std::vector<std::string>& names) {
  // The following names are used to create Tango attributes
  names.clear();
  names.push_back("Strength");
  names.push_back("Strength_H");
  names.push_back("Strength_V");
  names.push_back("Strength_SQ");
}

void SH5Magnet::get_strength_units(std::vector<std::string>& units) {
  // The following units are used to create Tango attributes properties
  units.clear();
  units.push_back("m-2");
  units.push_back("rad");
  units.push_back("rad");
  units.push_back("m-1");
}

//-----------------------------------------------------
// Compute strength(s) from current(s) in standard unit
void SH5Magnet::compute_strengths(double magnet_rigidity_inv,std::vector<double>& in_currents,std::vector<double>& out_strengths) {

  if (in_currents.size() != 5)
    throw std::invalid_argument(get_magnet_type()+"::compute_strengths() 5 currents expected");

  // Compute pseudo currents
  auto mapped_currents = Eigen::Map<Eigen::VectorXd>(in_currents.data(), in_currents.size());
  Eigen::VectorXd pI = M * mapped_currents;
  double hSign = (pI(0)<0.0)?-1.0:1.0;
  double vSign = (pI(1)<0.0)?-1.0:1.0;
  double sqSign = (pI(2)<0.0)?-1.0:1.0;
  double sxSign = (pI(3)<0.0)?-1.0:1.0;

  // Compute strengths
  out_strengths.resize(4);
  out_strengths[0] = -sxSign*get_excitation_linear(fabs(pI(3))) * magnet_rigidity_inv;
  out_strengths[1] = hSign*h.get_excitation_linear(fabs(pI(0))) * magnet_rigidity_inv;
  out_strengths[2] = -vSign*v.get_excitation_linear(fabs(pI(1))) * magnet_rigidity_inv;
  out_strengths[3] = -sqSign*sq.get_excitation_linear(fabs(pI(2))) * magnet_rigidity_inv;

}

//-----------------------------------------------------
// Compute current(s) from strength(s)
void SH5Magnet::compute_currents(double magnet_rigidity,std::vector<double>& in_strengths,std::vector<double>& out_currents) {

  if (in_strengths.size() != 4)
    throw std::invalid_argument(get_magnet_type()+"::compute_currents() 4 strengths expected");

  // Compute pseudo currents
  Eigen::Vector4d pI;
  double sxStrength = in_strengths[0] * magnet_rigidity;
  double hStrength = in_strengths[1] * magnet_rigidity;
  double vStrength = in_strengths[2] * magnet_rigidity;
  double sqStrength = in_strengths[3] * magnet_rigidity;
  double sxSign = (sxStrength<0.0)?-1.0:1.0;
  double hSign = (hStrength<0.0)?-1.0:1.0;
  double vSign = (vStrength<0.0)?-1.0:1.0;
  double sqSign = (sqStrength<0.0)?-1.0:1.0;
  pI(0) = hSign*h.solve_main_strength_linear(fabs(hStrength));
  pI(1) = -vSign*v.solve_main_strength_linear(fabs(vStrength));
  pI(2) = -sqSign*sq.solve_main_strength_linear(fabs(sqStrength));
  pI(3) = -sxSign*solve_main_strength_linear(fabs(sxStrength));

  out_currents.resize(5);
  Eigen::Map<Eigen::VectorXd>(out_currents.data(),out_currents.size()) = Mp * pI;

}

//-----------------------------------------------------
// Compute pseudo current(s) from current(s) in standard unit
void SH5Magnet::compute_pseudo_currents_from_currents(std::vector<double>& in_currents,std::vector<double>& out_currents) {

  auto mapped_currents = Eigen::Map<Eigen::VectorXd>(in_currents.data(), in_currents.size());
  out_currents.resize(4);
  Eigen::Map<Eigen::VectorXd>(out_currents.data(),out_currents.size()) = M * mapped_currents;

}

//-----------------------------------------------------
// Compute strength from pseudo currents (SH Only, idx=0 for H,idx=0 for V,idx=0 for SQ,idx=3 for SEXT)
void SH5Magnet::compute_strength_from_pseudo(double magnet_rigidity_inv,int idx,double& in_current,double& out_strength) {

  double sign = (in_current<0.0)?-1.0:1.0;
  switch(idx) {
    case 0:
      out_strength = sign * h.get_excitation_linear(fabs(in_current)) * magnet_rigidity_inv;
      break;
    case 1:
      out_strength = -sign * v.get_excitation_linear(fabs(in_current)) * magnet_rigidity_inv;
      break;
    case 2:
      out_strength = -sign * sq.get_excitation_linear(fabs(in_current)) * magnet_rigidity_inv;
      break;
    case 3:
      out_strength = -sign * get_excitation_linear(fabs(in_current)) * magnet_rigidity_inv;
      break;
    default:
      throw std::invalid_argument(get_magnet_type()+"::compute_strength_from_pseudo() invalid index");
  }

}


} // end namespace MagnetModel