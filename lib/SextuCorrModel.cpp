#include "SextuCorrModel.h"

namespace MagnetModel {

//-----------------------------------------------------
// Construtor
SextuCorrModel::SextuCorrModel() {
	corr_type = "a1";
	sym_matrix = sym_a1();
	linear_forms.clear();
	initialized = false;
}
//-----------------------------------------------------
// Construtor
// s = "a1", "b1", "a2" or "b3"
SextuCorrModel::SextuCorrModel(std::string s) {

	// Set corrector type and symmetry matrix
	if (s == "a1") {
		corr_type = s;
		sym_matrix = sym_a1();
	} else if (s == "b1") {
		corr_type = s;
		sym_matrix = sym_b1();
	} else if (s == "a2") {
		corr_type = s;
		sym_matrix = sym_a2();
	} else if (s == "b3") {
		corr_type = s;
		sym_matrix = sym_b3();
	}
	else corr_type = "unknown";

	// Reset linear forms
	linear_forms.clear();
	initialized = false;
}

//-----------------------------------------------------
// Set the corrector type
int SextuCorrModel::set_corr_type(std::string s)
{
	corr_type = s;
	return 0;
}

//-----------------------------------------------------
// Get the corrector type
std::string SextuCorrModel::get_corr_type()
{
	return corr_type;
}

//-----------------------------------------------------
// Set the symmetry matrix
int SextuCorrModel::set_sym_matrix(Eigen::MatrixXd m)
{
	sym_matrix = m;
	return 0;
}

//-----------------------------------------------------
// Get the symmetry matrix
Eigen::MatrixXd SextuCorrModel::get_sym_matrix()
{
	return sym_matrix;
}

//-----------------------------------------------------
// Get the symmetry matrix using references
int SextuCorrModel::get_sym_matrix(Eigen::MatrixXd& m)
{
	m = sym_matrix;
	return 0;
}

//-----------------------------------------------------
// Build a main current vector from the measured strengths
int SextuCorrModel::set_all_main_currents(std::vector<std::vector <double>> m_strengths) {
	
	// Initialize
	int n = m_strengths.size() / 8;
	int i;
	all_main_currents.clear();

	// Read all main currents in m_strengths
	for (i = 0; i < n; i++)
		all_main_currents.push_back(m_strengths[8 * i][0]);

	return 0;
}

//-----------------------------------------------------
// Return the vector of main currents
std::vector<double> SextuCorrModel::get_all_main_currents() 
{
	return all_main_currents;
}

//-----------------------------------------------------
// Symmetry matrix for skew dipole
Eigen::Matrix<double, 13, 13> SextuCorrModel::sym_a1() {

	// Build the symmetry matrix for the a_1 corrector
	Eigen::Matrix<double, 13, 13> mat;

	// Initialize
	mat << 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,	// i1
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,		// i2
		-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,		// i3		// GLB 28/05/2019	Sign corrected
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,		// i4
		0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0,		// i1^2
		0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0,		// i1 * i2
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,		// i1 * i3
		0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0,		// i1 * i4
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,		// i2^2		// GLB 28/05/2019	Set to zero (symmetry)
		0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0,		// i2 * i3	// GLB 28/05/2019	Sign corrected
		0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0,		// i3^2
		0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0,		// i3 * i4
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;		// i4^2		// GLB 28/05/2019	Set to zero (symmetry)

	// Return the matrix
	return mat;
}

//-----------------------------------------------------
// Symmetry matrix for normal dipole
Eigen::Matrix<double, 13, 13> SextuCorrModel::sym_b1() {

	// Build the symmetry matrix for the b_1 corrector
	Eigen::Matrix<double, 13, 13> mat;

	// Initialize
	mat << 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,	// i1
		0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,		// i2
		1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,		// i3		// GLB 28/05/2019	Sign corrected
		0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,		// i4
		0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0,		// i1^2
		0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0,		// i1 * i2
		0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0,		// i1 * i3
		0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0,		// i1 * i4
		0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0,		// i2^2
		0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0,		// i2 * i3	// GLB 28/05/2019	Sign corrected 
		0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0,		// i3^2
		0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0,		// i3 * i4
		0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0;		// i4^2

	// Return the matrix
	return mat;
}

//-----------------------------------------------------
// Symmetry matrix for skew quadrupole
Eigen::Matrix<double, 13, 13> SextuCorrModel::sym_a2() {

	// Build the symmetry matrix for the a_2 corrector
	Eigen::Matrix<double, 13, 13> mat;

	// Initialize
	mat << 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,	// i1
		0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,		// i2
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,		// i3
		0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,		// i4
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,		// i1^2
		0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0,		// i1 * i2
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,		// i1 * i3
		0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0,		// i1 * i4	// GLB 28/05/2019	Sign corrected
		0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0,		// i2^2		// GLB 28/05/2019	Sign corrected
		0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0,		// i2 * i3	// GLB 28/05/2019	Sign corrected
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,		// i3^2
		0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0,		// i3 * i4	// GLB 28/05/2019	Sign corrected
		0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0;		// i4^2		// GLB 28/05/2019	Sign corrected

	// Return the matrix
	return mat;
}

//-----------------------------------------------------
// Symmetry matrix for normal sextupole
Eigen::Matrix<double, 13, 13> SextuCorrModel::sym_b3() {

	// Build the symmetry matrix for the b_3 corrector
	Eigen::Matrix<double, 13, 13> mat;

	// Initialize
	mat << 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,		// i1
		0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,			// i2
		1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,			// i3		// GLB 28/05/2019	Sign corrected
		0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,			// i4
		0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0,			// i1^2
		0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0,			// i1 * i2
		0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0,			// i1 * i3
		0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0,			// i1 * i4
		0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0,			// i2^2
		0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0,			// i2 * i3
		0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0,			// i3^2
		0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0,			// i3 * i4
		0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0;			// i4^2		// GLB 28/05/2019	Sign corrected

	// Return the matrix
	return mat;
}

//-----------------------------------------------------
// Fill a row vector of linear and quadratic terms from the corrector currents
Eigen::Matrix<double, 1, 13> SextuCorrModel::current_vector(std::vector<double> c_curr) {

	 Eigen::Matrix<double, 1, 13> v_curr;
	 current_vector(c_curr, v_curr);

	// Return 
	return v_curr;
}

//-----------------------------------------------------
// Same as above, using references
// All computations are described here
int SextuCorrModel::current_vector(const std::vector<double>& c_curr, Eigen::Matrix<double, 1, 13>& v_curr) {

	// Initialize the vector with the corrector currents
	double i1 = c_curr[0];
	double i2 = c_curr[1];
	double i3 = c_curr[2];
	double i4 = c_curr[3];
	v_curr << i1, i2, i3, i4, i1*i1, i1*i2, i1*i3, i1*i4, i2*i2, i2*i3, i3*i3, i3*i4, i4*i4;			// i2*i4 term suppressed

	return 0;
}

//-----------------------------------------------------
// Build a matrix from all corrector currents at a given main current
// Each row include linear and quadratic terms for a given current set
int SextuCorrModel::current_matrix(int k, const std::vector<std::vector <double>>& m_strengths, Eigen::Matrix<double, 7, 13>& m_curr) {

	// Initialize
	int i;
	int ret = 0;
	double i0;
	std::vector<double> c_curr = {0, 0, 0, 0};

	// Fill the matrix with the currents
	i0 = m_strengths[8 * k][0]; // main current
	for (i = 1; i < 8; i++) {
		// Check the main current
		if (m_strengths[8 * k + i][0] != i0) {
			std::cout << "Warning: Main current differs from reference." << std::endl;
			ret = -1;
		}
		else {
			// Corrector currents, channels 1 to 4
			c_curr[0] = m_strengths[8 * k + i][1];
			c_curr[1] = m_strengths[8 * k + i][2];
			c_curr[2] = m_strengths[8 * k + i][3];
			c_curr[3] = m_strengths[8 * k + i][4];
		}
		// The ith matrix row is the current vector
		m_curr.row(i - 1) = current_vector(c_curr);
	}

	return ret;

}

//-----------------------------------------------------
// Build a matrix from all corrector currents at a given main current
// Same as above, return values
Eigen::Matrix<double, 7, 13> SextuCorrModel::current_matrix(int k, const std::vector<std::vector <double>>& m_strengths) {

	// Initialize
	Eigen::Matrix<double, 7, 13> m_curr;

	// Current matrix
	current_matrix(k, m_strengths, m_curr);

	// Return the current matrix
	return m_curr;
}

//-----------------------------------------------------
// Build a column vector from the measured multipole strengths at main current k
Eigen::MatrixXd SextuCorrModel::multipole_vector(int k, const std::vector<std::vector <double>>& m_strengths) {

	// Initialize
	int i, j, l = 0;
	Eigen::Matrix<double, 7, 1> v;
	bool corr_type_valid = true;

	// Select the data according to the corrector type
	if (corr_type == "a1") j = 1;
	else if (corr_type == "b1") j = 2;
	else if (corr_type == "a2") j = 3;
	else if (corr_type == "b3") { j = 4; l = 1; }
	else { corr_type_valid = false; std::cout << "Error: Unknown corrector type" << std::endl; }

	// Fill the vector with the strengths
	if (corr_type_valid) 
		for (i = 1; i < 8; i++) 
			v[i - 1] = m_strengths[8 * k + i][4 + j] - l * m_strengths[8 * k][8];
	else
		for (i = 1; i < 8; i++)
			v[i - 1] = 0;

	// Return the strengths
	return v;

}

//-----------------------------------------------------
// Build the linear form at main current k
Eigen::MatrixXd SextuCorrModel::build_linear_form(int k, const std::vector<std::vector <double>>& m_strengths) {

	// Strength vector
	Eigen::MatrixXd strength = multipole_vector(k, m_strengths);

	// Current matrix
	Eigen::MatrixXd curr_mat = current_matrix(k, m_strengths) * sym_matrix;

	// Compute the linear form
	Eigen::MatrixXd lf = sym_matrix * curr_mat.bdcSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(strength);

	// Return the linear form at main current k
	return lf.transpose();
}

//-----------------------------------------------------
// Build the linear forms for all currents
int SextuCorrModel::build_linear_forms_all_currents(const std::vector<std::vector <double>>& m_strengths) {

	// Initialize
	int n = m_strengths.size() / 8;
	int i;
	linear_forms.clear();

	// Linear forms at all currents k
	for (i = 0; i < n; i++)	
		linear_forms.push_back(build_linear_form(i, m_strengths));

	// Return 0 if no error
	return 0;	
}

//-----------------------------------------------------
// Build and interpole all the linear forms
int SextuCorrModel::initialize_model(const std::vector<std::vector <double>>& m_strengths) {

	// Main currents 
	set_all_main_currents(m_strengths);

	// Linear forms at all currents
	build_linear_forms_all_currents(m_strengths);

	// Interpolation functions
	NInterpolation n_interp(all_main_currents, linear_forms);
	interp_linear_forms = n_interp;

	// Set to initialized state
	initialized = true;

	// Return 0 if no error
	return 0;
}

//-----------------------------------------------------
// Interpolate the linear form at a given main current
Eigen::MatrixXd SextuCorrModel::interpolated_linear_form(double curr) {

	return interp_linear_forms.spline(curr);

}


//-----------------------------------------------------
// Same as above, using references
int SextuCorrModel::interpolated_linear_form(double curr, Eigen::MatrixXd& lf) {

	if (!initialized) return -1;

	interp_linear_forms.spline(curr, lf);

	return 0;
}

} // end namespace MagnetModel