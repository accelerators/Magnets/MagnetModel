#pragma once

#include <vector>
#include <iostream>
#include <Eigen/Dense>

namespace MagnetModel {

// --- Single interpolation function
class Interpolation {

public:
	// Constructors
	Interpolation();
	Interpolation(std::vector<double> x, std::vector<double> y);
	// Set points
	int set_points(std::vector<double> x, std::vector<double> y);
	// Sort the points in increasing x order
	void sort_points(std::vector<std::vector<double>> & v);
	// Initialize the coefficients for linear interpolation
	int set_linear_coefs();
	// Initialize the coefficients for spline interpolation
	int set_spline_coefs();
	// Linear interpolation
	double linear(double x);
	// Spline interpolation
	double spline(double x);
	// Derivative of the linear interpolation
	double linear_derivative(double x);
	// Derivative of the spline interpolation
	double spline_derivative(double x);
	// Return inverse curve
	Interpolation inverse();
	
private:
	// Find the interval [x_i, x_i+1]
	int find_interval(double x);
	std::vector<std::vector<double>> points;		// ((x_0, y_0), (x_1, y_1), ...)
	std::vector<double> linear_coefs;				// Coefficients (slopes) for linear interpolation
	std::vector<std::vector<double>> spline_coefs;	// Coefficients for spline interpolation
};

// --- Interpolate several functions evaluated at the same x value,
//        ie a N dimensional function of x
class NInterpolation {

public:
	// Constructors
	NInterpolation();
	NInterpolation(std::vector<double> x, std::vector<Eigen::MatrixXd> y);
	// Set points
	int set_points(std::vector<double> x, std::vector<Eigen::MatrixXd> y);
	// Linear interpolation
	Eigen::MatrixXd linear(double x);
	int linear(double x, Eigen::MatrixXd& m);
	// Spline interpolation
	Eigen::MatrixXd spline(double x);
	int spline(double x, Eigen::MatrixXd & m);

private:
	std::vector<double> x_values;				// x values
	std::vector<Eigen::MatrixXd> y_values;		// y values
	std::vector<Interpolation> n_interpolation;	// interpolation functions
	int r;										// number rows
	int c;										// number of columns
};

} // end namespace MagnetModel
