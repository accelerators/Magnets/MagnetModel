#include "Multipole.h"

namespace MagnetModel {

//-----------------------------------------------------
// Constructor 
Multipole::Multipole()
{
	radius = 10;				// Reference radius
	an.assign(1, 0);			// Skew multipoles
	bn.assign(1, 0);			// Normal multipoles
}
//-----------------------------------------------------
// Constructor 
Multipole::Multipole(int n)
{
	radius = 10;				// Reference radius
	an.assign(n, 0);			// Skew multipoles
	bn.assign(n, 0);			// Normal multipoles
}
//-----------------------------------------------------
// Constructor 
Multipole::Multipole(int n, double r)
{
	radius = r;					// Reference radius
	an.assign(n, 0);			// Skew multipoles
	bn.assign(n, 0);			// Normal multipoles
}

//-----------------------------------------------------
// Get the reference radius
double Multipole::get_radius() {
	return radius;
}

//-----------------------------------------------------
// Get the reference radius
int Multipole::set_radius(double r) {
	radius = r;
	return 0;
}

//-----------------------------------------------------
// Get the skew multipoles  
std::vector<double> Multipole::get_an() {
	return an;
}

//-----------------------------------------------------
// Get the skew multipoles  
int Multipole::get_an(std::vector<double>& a) {
	a = an;
	return 0;
}
//-----------------------------------------------------
// Get the normal multipoles  
std::vector<double> Multipole::get_bn() {
	return bn;
};

//-----------------------------------------------------
// Get the normal multipoles  
int Multipole::get_bn(std::vector<double>& b) {
	b = bn;
	return 0;
}

//-----------------------------------------------------
// Get the normal and skew  multipoles  
int Multipole::get_an_bn(std::vector<double>& a, std::vector<double>& b) {
	a = an;
	b = bn;
	return 0;
};

//-----------------------------------------------------
// Set the skew multipoles
int Multipole::set_an(const std::vector<double>& a) {
	an = a;
	return 0;
}

//-----------------------------------------------------
// Set the skew multipoles
int Multipole::set_bn(const std::vector<double>& b) {
	bn = b;
	return 0;
};

// ---------------------------------------------------- -
// Set the normal and skew  multipoles  
int Multipole::set_an_bn(const std::vector<double>& a, const std::vector<double>& b) {
	an = a;
	bn = b;
	return 0;
};

} // end namespace MagnetModel