#pragma once

#include <Eigen/Dense>
#include <Eigen/SVD>
#include <Eigen/LU>
#include <iostream>
#include <vector>
#include "Interpolation.h"

namespace MagnetModel {

class SextuCorrModel {

public:
	// --- Constructors
	SextuCorrModel();
	SextuCorrModel(std::string s);
	// --- Set and get attributes
	// Corrector type
	int set_corr_type(std::string s);
	std::string get_corr_type();
	// Symmetry matrix
	int set_sym_matrix(Eigen::MatrixXd m);
	Eigen::MatrixXd get_sym_matrix();
	int get_sym_matrix(Eigen::MatrixXd& m);
	// Set all main current values
	int set_all_main_currents(std::vector<std::vector <double>> m_strengths);
	std::vector<double> get_all_main_currents();
	// --- Symmetry matrices (a1 for skew dipole, b1 for normal dipole, etc.)
	Eigen::Matrix<double, 13, 13> sym_a1();
	Eigen::Matrix<double, 13, 13> sym_b1();
	Eigen::Matrix<double, 13, 13> sym_a2();
	Eigen::Matrix<double, 13, 13> sym_b3();
	// --- Corrector current vector and matrix
	// Fill a row vector of linear and quadratic terms from the corrector currents
	Eigen::Matrix<double, 1, 13> current_vector(std::vector<double> c_curr);
	int current_vector(const std::vector<double>& c_curr, Eigen::Matrix<double, 1, 13>& v_curr);
	Eigen::Matrix<double, 7, 13> current_matrix(int k, const std::vector<std::vector <double>>& m_strengths);
	int current_matrix(int k, const std::vector<std::vector <double>>& m_strengths, Eigen::Matrix<double, 7, 13>& m_curr);
	// --- Multipole vector
	Eigen::MatrixXd multipole_vector(int k, const std::vector<std::vector <double>>& m_strengths);
	// --- Build the models at main current k and for all main currents
	Eigen::MatrixXd build_linear_form(int k, const std::vector<std::vector <double>>& m_strengths);
	int build_linear_forms_all_currents(const std::vector<std::vector <double>>& m_strengths);
	int initialize_model(const std::vector<std::vector <double>>& m_strengths);
	// --- Interpolated linear form
	Eigen::MatrixXd interpolated_linear_form(double curr);
	int interpolated_linear_form(double curr, Eigen::MatrixXd& lf);

private:
	std::string corr_type;						// Type of corrector (a1 for skew dipole, b1 for normal dipole, etc.)
	Eigen::MatrixXd sym_matrix;					// symmetry matrix
	std::vector<Eigen::MatrixXd> linear_forms;	// Container of linear forms at given main current
	std::vector<double> all_main_currents;		// Contains all values of the main current
	NInterpolation interp_linear_forms;			// Interpolation functions for linear forms
	bool initialized;							// True if the model is initialized
};

} // end namespace MagnetModel