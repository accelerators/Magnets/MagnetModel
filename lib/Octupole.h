#pragma once

#include "Magnet.h"

namespace MagnetModel {

class Octupole: public Magnet {

public:
  // Constructor and initialization
  Octupole() : Magnet(0, 0, 0, 0) {}
  int init(bool focusing,double scale,std::string strength_file_name, std::string param_file_name, std::string mag_s_n);

  // Implementation of virtual methods (described in Magnet.h)
  bool has_main_current();
  void get_strength_names(std::vector<std::string>& names);
  virtual void get_strength_units(std::vector<std::string>& units);
  void compute_strengths(double magnet_rigidity_inv,std::vector<double>& in_currents,std::vector<double>& out_strengths);
  void compute_currents(double magnet_rigidity,std::vector<double>& in_strengths,std::vector<double>& out_currents);
  void compute_pseudo_currents_from_currents(std::vector<double>& in_currents,std::vector<double>& out_currents) {};
  void compute_strength_from_pseudo(double magnet_rigidity_inv,int idx,double& in_current,double& out_strength) {};

protected:
  bool focusing;

};

} // end namespace MagnetModel