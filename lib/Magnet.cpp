#include "Magnet.h"

namespace MagnetModel {

//-----------------------------------------------------
// Constructor 
Magnet::Magnet()
{
	current_main = 0;	
	current_corr.assign(1, 0);	
	Multipole multi;
	multipole = multi;
	magnet_type = "unknown";
	solver_max_iter = 20;
	solver_tolerance = 1e-9;
	scale = 1.0;
	offset = 0.0;
}

//-----------------------------------------------------
// Constructor 
Magnet::Magnet(double curr_main, int n_corr, int n_multi)
{
	current_main = curr_main;	
	current_corr.assign(n_corr, 0);	
	Multipole multi(n_multi);
	multipole = multi;
	magnet_type = "unknown";
	solver_max_iter = 20;
	solver_tolerance = 1e-9;
	scale = 1.0;
	offset = 0.0;
}

//-----------------------------------------------------
// Constructor 
Magnet::Magnet(double curr_main, int n_corr, int n_multi, double r)
{
	current_main = curr_main;	
	current_corr.assign(n_corr, 0);	
	Multipole multi(n_multi, r);
	multipole = multi;
	magnet_type = "unknown";
	solver_max_iter = 20;
	solver_tolerance = 1e-9;
	scale = 1.0;
	offset = 0.0;
}

//-----------------------------------------------------
// Set main magnet current [A]
int Magnet::set_current_main(double curr_main) 
{
	current_main = curr_main;
	return 0;
}

//-----------------------------------------------------
// Get main magnet current [A]
double Magnet::get_current_main() 
{
	return current_main;
}

//-----------------------------------------------------
// Set corrector magnet currents [A]
int Magnet::set_current_corr(const std::vector<double>& curr_corr)
{
	current_corr = curr_corr;
	return 0;
}

//-----------------------------------------------------
// Get corrector magnet currents [A]
std::vector<double> Magnet::get_current_corr() 
{
	return current_corr;
}

//-----------------------------------------------------
// Return corrector number
size_t Magnet::get_corrector_number() {
	return current_corr.size();
}

//-----------------------------------------------------
// Same as above, using references
int Magnet::get_current_corr(std::vector<double>& curr_corr) {
	
	curr_corr = current_corr;
	return 0;
}

//-----------------------------------------------------
// Set magnet type
int Magnet::set_magnet_type(std::string m_type_str)
{
	magnet_type = m_type_str;
	return 0;
}

//-----------------------------------------------------
// Get magnet type
std::string Magnet::get_magnet_type()
{
	return magnet_type;
}

//-----------------------------------------------------
// Set multipoles
int Magnet::set_multipoles(const Multipole& multi) 
{
	multipole = multi;
	return 0;
}
//-----------------------------------------------------
// Get multipoles
Multipole Magnet::get_multipoles() 
{
	return multipole;
}
//-----------------------------------------------------
// Get multipoles
int Magnet::get_multipoles(Multipole& multi)
{
	multi = multipole;
	return 0;
}

//-----------------------------------------------------
// Set the multipole strengths from the a and b vector
int Magnet::set_multipole_strengths(const std::vector<double>& a, const std::vector<double>& b) 
{
	multipole.set_an(a);
	multipole.set_bn(b);
	return 0;
}

//-----------------------------------------------------
// Set the magnet scale
int Magnet::set_scale(double s)
{
	scale = s;
	return 0;
}
//-----------------------------------------------------
// Get the magnet scale
double Magnet::get_scale()
{
	return scale;
}

//-----------------------------------------------------
// Set the magnet offset
int Magnet::set_offset(double o)
{
	offset = o;
	return 0;
}

//-----------------------------------------------------
// Get the magnet offset
double Magnet::get_offset()
{
	return offset;
}

//-----------------------------------------------------
// pseudo current offset (for DQ mdoel)
double Magnet::get_pseudo_current_offset() {
  return pseudo_current_offset;
}


//-----------------------------------------------------
// Load a matrix from a file
int Magnet::load_matrix(std::string file_name,int row,int col,Eigen::MatrixXd &m) {

  // --- Init
  std::ifstream file;

  // --- open parameter file
  file.open(file_name);
  // Check if file does not exist
  if (!file)
    throw std::invalid_argument(get_magnet_type()+"::load_matrix() "+file_name+" file not found");

  // --- Read the file
  int r = 0;
  std::vector<double> data;
  while (!file.eof() && r<row) {

    // Get a line from the file
    std::string line;
    std::getline(file,line);

    // Skip comment and empty line
    if(line.empty() || line[0]=='#')
      continue;

    std::vector<std::string> items;
    split(items,line,',');
    if(items.size()!=col) {
      file.close();
      throw std::invalid_argument(
              get_magnet_type()+"::load_matrix() " + file_name + " " + std::to_string(col) + " items per line expected");
    }
    try {
      for(int i=0;i<items.size();i++)
        data.push_back(std::stod(items[i]));
    } catch (std::exception& e) {
      throw std::invalid_argument(
              get_magnet_type()+"::load_matrix() " + file_name + " incorrect number in " + line);
    }

  }

  // --- Close the file
  file.close();

  if(data.size()!=row*col)
    throw std::invalid_argument(
            get_magnet_type()+"::load_matrix() " + file_name + " " + std::to_string(row) + "x" + std::to_string(col) + " matrix expected");

  // Set the matrix
  m = Eigen::Map<Eigen::Array<double, -1, -1, Eigen::RowMajor>>(data.data(),row,col);

  // --- Return without error
  return 0;

}

//-----------------------------------------------------
// Load an excitation curve
int Magnet::set_excitation_from_file(std::string file_name)
{
  // --- Init
  std::ifstream file;

  // --- open parameter file
  file.open(file_name);
  // Check if file does not exist
  if (!file)
    throw std::invalid_argument(get_magnet_type()+"::set_excitation_from_file() "+file_name+" file not found");

  // --- Read the file
  std::vector<double> x;
  std::vector<double> y;
  double scale = get_scale();
  double offset = get_offset();

  while (!file.eof()) {

    // Get a line from the file
    std::string line;
    std::getline(file,line);

    // Skip comment and empty line
    if(line.empty() || line[0]=='#')
      continue;

    std::vector<std::string> items;
    split(items,line,',');
    if(items.size()!=2) {
      file.close();
      throw std::invalid_argument(
              get_magnet_type()+"::set_excitation_from_file() " + file_name + " 2 items per line expected");
    }
    try {
      x.push_back(std::stod(items[0]));
      y.push_back(std::stod(items[1]) * scale + offset);
    } catch (std::exception& e) {
      throw std::invalid_argument(
              get_magnet_type()+"::set_excitation_from_file() " + file_name + " incorrect number in " + line);
    }

  }

  // --- Close the file
  file.close();

  // Set the excitation curve
  Interpolation ex_curve(x, y);
  set_excitation_curve(ex_curve);

  // --- Return without error
  return 0;

}

//-----------------------------------------------------
// Set the magnet parameters using a configuration file
int Magnet::set_params_from_file(std::string file_name, std::string mag_s_n) {
	
	// --- Init 
	std::ifstream file;
	std::string param;
	bool s_n_found = false;

	// --- open parameter file
	file.open(file_name);
	// Check if file does not exist
	if (!file)
		throw std::invalid_argument("Magnet::set_params_from_file() "+file_name+" file not found");

	while (!file.eof() && !s_n_found) {
		std::string line;
		std::getline(file,line);

		// Skip comment and empty line
		if(line.empty() || line[0]=='#')
			continue;

		std::vector<std::string> items;
		split(items,line,',');
		if(items.size()!=5) {
			file.close();
			throw std::invalid_argument("Magnet::set_params_from_file() " + file_name +
				" 5 items per line expected (serial,scale,offset,radius,pscurroffset)");
		}

		// Check the serial number
		if (!items[0].compare(mag_s_n)) {
			s_n_found = true;
			// Set serial number
			try {
				serial_number = mag_s_n; 
				// Read scale
				scale = std::stod(items[1]);
				// Read offset
				offset = std::stod(items[2]);
				// Read reference radius
				multipole.set_radius(std::stod(items[3]));
				// Read pseudo current offset
				pseudo_current_offset = std::stod(items[4]);
			} catch (std::exception& e) {
				throw std::invalid_argument( get_magnet_type()+"::set_params_from_file() " + file_name + " incorrect number in " + line);
			}

		}
	}

	// Close the file
	file.close();

	// If the specified S/N does not exits
	if (!s_n_found)
		throw std::invalid_argument("Magnet::set_params_from_file() "+mag_s_n+" not found in " + file_name);

	// --- Return without error
	return 0;
}

//-----------------------------------------------------
// Set the excitation curve
int Magnet::set_excitation_curve(Interpolation ex_curve) {

	excitation_curve = ex_curve;
	rexcitation_curve = ex_curve.inverse();
	return 0;
}
//-----------------------------------------------------
// Get the excitation curve
Interpolation Magnet::get_excitation_curve() {

	return excitation_curve;
}

//-----------------------------------------------------
// Get the excitation 
// WARNING: using this function, the impact of the corrector currents is not taken into account
double Magnet::get_excitation() {

	return excitation_curve.spline(current_main);
}

//-----------------------------------------------------
// Get the excitation for a given current
double Magnet::get_excitation(double curr_main) {

	return excitation_curve.spline(curr_main);

}

//-----------------------------------------------------
// Get the excitation 
// WARNING: using this function, the impact of the corrector currents is not taken into account
double Magnet::get_excitation_linear() {

	return excitation_curve.linear(current_main);
}

//-----------------------------------------------------
// Get the excitation for a given current
double Magnet::get_excitation_linear(double curr_main) {

	return excitation_curve.linear(curr_main);

}

//-----------------------------------------------------
// Solve the current for the given strength
double Magnet::solve_main_strength_linear(double target_strength) {
	return rexcitation_curve.linear(target_strength);
}

//-----------------------------------------------------
// Solve the current for the given strength using the Newton's method
double Magnet::solve_main_strength(double target_strength) {

	// Initial current
	double i_k = current_main;
	double strength_k;

	// Iterate
	for (int k = 0; k < solver_max_iter; k++) {
		// New value for the current
		strength_k = excitation_curve.spline(i_k);
		i_k -= (strength_k - target_strength) / excitation_curve.spline_derivative(i_k);
		// Convergence test
		if (abs(strength_k - target_strength) < solver_tolerance) break;
	}

	return i_k;

}

//-----------------------------------------------------
// Set the solver tolerance
int Magnet::set_solver_tolerance(double eps) {
	solver_tolerance = eps;
	return 0;
}

//-----------------------------------------------------
// Get the solver tolerance
double Magnet::get_solver_tolerance() {
	return solver_tolerance;
}

//-----------------------------------------------------
// Set the solver maximum number of iterations
int Magnet::set_solver_max_iterations(int max_iter) {
	solver_max_iter = max_iter;
	return 0;
}

//-----------------------------------------------------
// Get the solver maximum number of iterations
int Magnet::get_solver_max_iterations() {
	return solver_max_iter;
}

//-----------------------------------------------------
// Split given string into tokens using specified separator
std::string trim(const std::string &s)
{
  auto start = s.begin();
  while (start != s.end() && std::isspace(*start))
    start++;
  auto end = s.end();
  do {
    end--;
  } while (std::distance(start, end) > 0 && std::isspace(*end));

  return std::string(start, end + 1);
}

void Magnet::split(std::vector<std::string> &tokens, const std::string &text, char sep) {

  size_t start = 0, end = 0;
  tokens.clear();

  while ((end = text.find(sep, start)) != std::string::npos) {
    tokens.push_back(trim(text.substr(start, end - start)));
    start = end + 1;
  }

  tokens.push_back(trim(text.substr(start)));

}

} // end namespace MagnetModel