#pragma once

#include "Magnet.h"
#include "SextuCorrModel.h"

namespace MagnetModel {

class Sextupole : public Magnet {

public:
	// Constructor and initialization
	Sextupole() : Magnet(0, 4, 3, 10) {}
	int init(double curr_main, std::vector<double> curr_corr, double r);
	int init(double curr_main, std::vector<double> curr_corr, double r, std::string file_name);
	int init(std::string strength_file_name, std::string param_file_name, std::string mag_s_n);
	// Set the strengths from a file
	int set_meas_strengths_from_file(std::string file_name);	// Load measured strenghts (needed for building the NL model)
	// Apply scale and offset to meas_strengths
	int apply_scale_to_meas_strengths();
	// Get the strengths
	std::vector<std::vector <double>> get_meas_strengths();
	// Compute and set the excitation curve
	int set_excitation_curve_from_meas_strengths();
	// Set the corrector models and the excitation curve from a file
	int set_model_from_file(std::string file_name);
	// Compute the multipoles at a given set of currents
	int multipoles_from_currents(double curr_main, const std::vector<double>& curr_corr);
	// Compute and set the multipoles at a given set of currents
	int set_multipoles_from_currents(double curr_main, const std::vector<double>& curr_corr);
	// Linear solver for strengths -- Enforce db3_corr = 0
	int solve_linear(double a1, double b1, double a2, double b3);
	// Compute the jacobian matrix
	int jacobian_matrix(const std::vector<double>& current_corr, bool compute_linear_form=true);
	// Get the jacobian matrix
	int get_jacobian_matrix(Eigen::Matrix<double, 4, 4>& jacobian_matrix);
	// Non-linear solver for strengths, based on Newton's method -- Enforce db3_corr = 0
	int solve_newton(double a1, double b1, double a2, double b3);
	// Read the values of a1, b1, a2 and b3 and put it in a vector
	int extract_an_bn(Eigen::Matrix<double, 4, 1>& an_bn);
	// Get the computed current values
	int get_solver_results(double& curr_main, std::vector<double>& curr_corr);
	// Get the number of iterations before convergence of the last NL solve
	int get_iterations();
	// Compute the currents from the multipoles
	int set_currents_from_multipoles(Multipole& multi);
	// Set the solver type
	int set_solver(unsigned char s);
	// Get the solver type
	unsigned char get_solver_results();

  // Implementation of virtual methods (described in Magnet.h)
  bool has_main_current();
  void get_strength_names(std::vector<std::string>& names);
  void get_strength_units(std::vector<std::string>& units);
  void compute_strengths(double magnet_rigidity_inv,std::vector<double>& in_currents,std::vector<double>& out_strengths);
  void compute_currents(double magnet_rigidity,std::vector<double>& in_strengths,std::vector<double>& out_currents);
  void compute_pseudo_currents_from_currents(std::vector<double>& in_currents,std::vector<double>& out_currents) {};
  void compute_strength_from_pseudo(double magnet_rigidity_inv,int idx,double& in_current,double& out_strength) {};

private:
	std::vector<std::vector <double>> meas_strengths; // Measured strengths from parameter file
	SextuCorrModel a1_corr;				// Model for a1 channel
	SextuCorrModel b1_corr;				// Model for b1 channel
	SextuCorrModel a2_corr;				// Model for a2 channel
	SextuCorrModel b3_corr;				// Model for b3 channel
	Eigen::Matrix<double, 1, 13> calc_curr_vector;
	Eigen::MatrixXd calc_curr_vector_tr;
	Eigen::MatrixXd calc_linear_form_a1;
	Eigen::MatrixXd calc_linear_form_b1;
	Eigen::MatrixXd calc_linear_form_a2;
	Eigen::MatrixXd calc_linear_form_b3;
	Eigen::Matrix<double, 4, 1> calc_an_bn;
	Eigen::Matrix<double, 4, 1> calc_an_bn_target;
	Eigen::Matrix<double, 4, 4> calc_resp_matrix;
	Eigen::Matrix<double, 4, 4> calc_jacobian_mat;
	std::vector<double> calc_curr_corr;
	Eigen::Matrix<double, 4, 1> calc_curr_corr_mat;
	double calc_curr_main;
	Multipole calc_multi;
	int calc_iter;
	std::vector<double> calc_an;
	std::vector<double> calc_bn;
	unsigned char solver;
};

} // end namespace MagnetModel