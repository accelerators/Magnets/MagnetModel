#include "Sextupole.h"
#include "SextuCorrModel.h"

namespace MagnetModel {

//-----------------------------------------------------
// Init a sextupole 
int Sextupole::init(double curr_main, std::vector<double> curr_corr, double r) {
	
	// Magnet type
	set_magnet_type("sextupole");
	// Set currents
	set_current_main(curr_main);
	set_current_corr(curr_corr);
	// Multipoles
	Multipole multi(3, r); // multipoles a1 to b3 at reference radius r
	set_multipoles(multi);
	// Sextupole corrector models
	SextuCorrModel a1("a1");
	a1_corr = a1;
	SextuCorrModel b1("b1");
	b1_corr = b1;
	SextuCorrModel a2("a2");
	a2_corr = a2;
	SextuCorrModel b3("b3");
	b3_corr = b3;
	// Temporary values for an and bn, used during field computations
	calc_linear_form_a1.resize(1, 13);
	calc_linear_form_b1.resize(1, 13);
	calc_linear_form_a2.resize(1, 13);
	calc_linear_form_b3.resize(1, 13);
	calc_an.resize(3);
	calc_an[0] = calc_an[1] = calc_an[2] = 0;
	calc_bn.resize(3);
	calc_bn[0] = calc_bn[1] = calc_bn[2] = 0;
	calc_curr_corr.resize(4);
	// Default solver
	solver = 1;

	// Return without error
	return 0;
}
// Same as above + read parameters and init the corrector models
int Sextupole::init(double curr_main, std::vector<double> curr_corr, double r, std::string file_name) {

	// Initialize the sextupole
	init(curr_main, curr_corr, r);

	// Initialize the models
	return set_model_from_file(file_name);

}

// Same as above + read all parameters in files and all currents set to 0
int Sextupole::init(std::string strength_file_name, std::string param_file_name, std::string mag_s_n) {

	// Currents
	double curr_main = 0;
	std::vector<double> curr_corr = { 0, 0, 0, 0 };

	// Initialize the sextupole
	init(curr_main, curr_corr, 10);

	// Initialize scales, offsets, etc. 
	if (set_params_from_file(param_file_name, mag_s_n) != 0) return -1;

	// Initialize the corrector models
	if(set_model_from_file(strength_file_name) != 0 ) return -1;

	return 0;

}


//-----------------------------------------------------
// Read the measured strengths from a file
// Used at initialization
int Sextupole::set_meas_strengths_from_file(std::string file_name) 
{
	// --- Init 
	std::ifstream file;
	int i;
	std::vector<double> meas_line(9, 0);

	// --- open parameter file
	file.open(file_name);
	// Check if file does not exist
	if (!file)
    throw std::invalid_argument("Sextupole::set_meas_strengths_from_file() "+file_name+" file not found");

	// Clear existing values, if any
	meas_strengths.clear();
	
	// --- Read the file
	
	while (!file.eof()) {
		// Get a line from the file
    std::string line;
    std::getline(file,line);

    // Skip comment and empty line
    if(line.empty() || line[0]=='#')
      continue;

    std::vector<std::string> items;
    split(items,line,',');
    if(items.size()!=9) {
      file.close();
      throw std::invalid_argument(
              "Sextupole::set_meas_strengths_from_file() " + file_name + " 9 items per line expected");
    }
		for (i = 0; i < 9; i++)
					meas_line[i] = std::stod(items[i]);
		// Insert the line at the end
  	meas_strengths.push_back(meas_line);

	}
	
	// --- Close the file
	file.close();

	// --- Return without error
	return 0;

}

//-----------------------------------------------------
// Apply scale and offset to measured strengths
int Sextupole::apply_scale_to_meas_strengths() {

	// --- Get the scale and offset
	double scale = get_scale();
	double offset = get_offset();

	// --- Apply to the measured strengths
	size_t n = meas_strengths.size();
	for (size_t k = 0; k < n; k++) {
		meas_strengths[k][5] = scale * meas_strengths[k][5];
		meas_strengths[k][6] = scale * meas_strengths[k][6];
		meas_strengths[k][7] = scale * meas_strengths[k][7];
		meas_strengths[k][8] = offset + scale * meas_strengths[k][8];
	}

	return 0;
}

//-----------------------------------------------------
// Get the measured strengths
std::vector<std::vector <double>> Sextupole::get_meas_strengths() {
	
	return meas_strengths;
}

//-----------------------------------------------------
// Compute and set the excitation curve from the measured strengths
int Sextupole::set_excitation_curve_from_meas_strengths() {

	// Initialize
	std::vector<double> curr;
	std::vector<double> b3;
	curr.clear();
	b3.clear();
	int n = meas_strengths.size();
	int i;

	if (!n) return -1;

	// Read the excitation values in the meas_strengths
	for (i = 0; i < n; i += 8) {
		curr.push_back(meas_strengths[i][0]);
		b3.push_back(meas_strengths[i][8]);
	}

	// New excitation curve
	Interpolation ex_curve(curr, b3);
	
	// Set the excitation curve
	set_excitation_curve(ex_curve);

	// Return
	return 0;

}

//-----------------------------------------------------
// Set the corrector models and the excitation curve from a file
int Sextupole::set_model_from_file(std::string file_name) {

	// Read the strengths
	int i = set_meas_strengths_from_file(file_name);
	if (i) return i;

	// Apply the scale and offset
	apply_scale_to_meas_strengths();

	// Set the excitation curve
	set_excitation_curve_from_meas_strengths();

	// Set the corrector models
	a1_corr.initialize_model(meas_strengths);
	b1_corr.initialize_model(meas_strengths);
	a2_corr.initialize_model(meas_strengths);
	b3_corr.initialize_model(meas_strengths);

	// Return 
	return 0;
}

//-----------------------------------------------------
// Compute the multipoles from the currents
int Sextupole::multipoles_from_currents(double curr_main, const std::vector<double>& curr_corr) {

	// --- Corrector current vector with linear and quadratic terms
	a1_corr.current_vector(curr_corr, calc_curr_vector);
	calc_curr_vector_tr = calc_curr_vector.transpose();

	// --- Sextupole from excitation curve
	double b3_main = get_excitation(curr_main);

	// --- Compute the multipoles
	// - a1
	// Get linear form at main current
	if (a1_corr.interpolated_linear_form(curr_main, calc_linear_form_a1)) return -1;
	// Compute the strength
	calc_an[0] = (calc_linear_form_a1 * calc_curr_vector_tr)(0);

	// - b1
	// Get linear form at main current
	if (b1_corr.interpolated_linear_form(curr_main, calc_linear_form_b1)) return -1;
	// Get the symmetry matrix
	calc_bn[0] = (calc_linear_form_b1 * calc_curr_vector_tr)(0);

	// - a2
	// Get linear form at main current
	if (a2_corr.interpolated_linear_form(curr_main, calc_linear_form_a2)) return -1;
	// Compute the strength
	calc_an[1] = (calc_linear_form_a2 * calc_curr_vector_tr)(0);

	// - b3
	// Get linear form at main current
	if (b3_corr.interpolated_linear_form(curr_main, calc_linear_form_b3)) return -1;
	// Get the symmetry matrix
	calc_bn[2] = b3_main + (calc_linear_form_b3 * calc_curr_vector_tr)(0);

	return 0;
}

//-----------------------------------------------------
// Set the multipoles and the currents from the specified currents
int Sextupole::set_multipoles_from_currents(double curr_main, const std::vector<double>& curr_corr) {

	
	// --- Compute the multipoles
	if (multipoles_from_currents(curr_main, curr_corr)) return -1;

	// --- Set the currents Currents
	set_current_main(curr_main);
	set_current_corr(curr_corr);

	// --- Set the multipoles
	set_multipole_strengths(calc_an, calc_bn);

	return 0;
}

//-----------------------------------------------------
// Linear solver for the specified multipole strengths
// Enforce a zero contribution of the corrector to b3
// Non-linear terms are neglected: to be used for initialization only
int Sextupole::solve_linear(double a1, double b1, double a2, double b3) {

	// --- Compute the initial point
	// Build the target matrix
	calc_an_bn_target << a1, b1, a2, 0;

	// Compute the main current
	calc_curr_main = solve_main_strength(b3);

	// Compute all linear forms at the initial main current
	a1_corr.interpolated_linear_form(calc_curr_main, calc_linear_form_a1);
	b1_corr.interpolated_linear_form(calc_curr_main, calc_linear_form_b1);
	a2_corr.interpolated_linear_form(calc_curr_main, calc_linear_form_a2);
	b3_corr.interpolated_linear_form(calc_curr_main, calc_linear_form_b3);

	// Compute the response matrix at the initial main current
	calc_resp_matrix.row(0) << calc_linear_form_a1(0), calc_linear_form_a1(1), calc_linear_form_a1(2), calc_linear_form_a1(3);
	calc_resp_matrix.row(1) << calc_linear_form_b1(0), calc_linear_form_b1(1), calc_linear_form_b1(2), calc_linear_form_b1(3);
	calc_resp_matrix.row(2) << calc_linear_form_a2(0), calc_linear_form_a2(1), calc_linear_form_a2(2), calc_linear_form_a2(3);
	calc_resp_matrix.row(3) << calc_linear_form_b3(0), calc_linear_form_b3(1), calc_linear_form_b3(2), calc_linear_form_b3(3);

	// Compute the initial values of the corrector currents
	calc_curr_corr_mat = calc_resp_matrix.inverse() * calc_an_bn_target;
	
	// Set the values
	calc_curr_corr[0] = calc_curr_corr_mat(0);
	calc_curr_corr[1] = calc_curr_corr_mat(1);
	calc_curr_corr[2] = calc_curr_corr_mat(2);
	calc_curr_corr[3] = calc_curr_corr_mat(3);

	return 0;
}

//-----------------------------------------------------
// Compute the jacobian matrix 
int Sextupole::jacobian_matrix(const std::vector<double>& current_corr, bool compute_linear_form) {

	// Extract the corrector currents 
	double i1 = current_corr[0];
	double i2 = current_corr[1];
	double i3 = current_corr[2];
	double i4 = current_corr[3];

	// Compute all linear forms at the initial main current
	if (compute_linear_form) {
		a1_corr.interpolated_linear_form(calc_curr_main, calc_linear_form_a1);
		b1_corr.interpolated_linear_form(calc_curr_main, calc_linear_form_b1);
		a2_corr.interpolated_linear_form(calc_curr_main, calc_linear_form_a2);
		b3_corr.interpolated_linear_form(calc_curr_main, calc_linear_form_b3);
	}

	// Jacobian matrix
	// Row 0: da_1/di_1, da_1/di_2, da_1/di_3, da_1/di_4
	calc_jacobian_mat(0, 0) = calc_linear_form_a1(0) + 2 * calc_linear_form_a1(4) * i1 + calc_linear_form_a1(5) * i2 + calc_linear_form_a1(6) * i3 + calc_linear_form_a1(7) * i4;
	calc_jacobian_mat(0, 1) = calc_linear_form_a1(1) + calc_linear_form_a1(5) * i1 + 2 * calc_linear_form_a1(8) * i2 + calc_linear_form_a1(9) * i3;
	calc_jacobian_mat(0, 2) = calc_linear_form_a1(2) + calc_linear_form_a1(6) * i1 + calc_linear_form_a1(9) * i2 + 2 * calc_linear_form_a1(10) * i3 + calc_linear_form_a1(11) * i4;
	calc_jacobian_mat(0, 3) = calc_linear_form_a1(3) + calc_linear_form_a1(7) * i1 + calc_linear_form_a1(11) * i3 + 2 * calc_linear_form_a1(12) * i4;
	// Row 1: db_1/di_1, db_1/di_2, db_1/di_3, db_1/di_4
	calc_jacobian_mat(1, 0) = calc_linear_form_b1(0) + 2 * calc_linear_form_b1(4) * i1 + calc_linear_form_b1(5) * i2 + calc_linear_form_b1(6) * i3 + calc_linear_form_b1(7) * i4;
	calc_jacobian_mat(1, 1) = calc_linear_form_b1(1) + calc_linear_form_b1(5) * i1 + 2 * calc_linear_form_b1(8) * i2 + calc_linear_form_b1(9) * i3;
	calc_jacobian_mat(1, 2) = calc_linear_form_b1(2) + calc_linear_form_b1(6) * i1 + calc_linear_form_b1(9) * i2 + 2 * calc_linear_form_b1(10) * i3 + calc_linear_form_b1(11) * i4;
	calc_jacobian_mat(1, 3) = calc_linear_form_b1(3) + calc_linear_form_b1(7) * i1 + calc_linear_form_b1(11) * i3 + 2 * calc_linear_form_b1(12) * i4;
	// Row 2: da_2/di_1, da_2/di_2, da_2/di_3, da_2/di_4
	calc_jacobian_mat(2, 0) = calc_linear_form_a2(0) + 2 * calc_linear_form_a2(4) * i1 + calc_linear_form_a2(5) * i2 + calc_linear_form_a2(6) * i3 + calc_linear_form_a2(7) * i4;
	calc_jacobian_mat(2, 1) = calc_linear_form_a2(1) + calc_linear_form_a2(5) * i1 + 2 * calc_linear_form_a2(8) * i2 + calc_linear_form_a2(9) * i3;
	calc_jacobian_mat(2, 2) = calc_linear_form_a2(2) + calc_linear_form_a2(6) * i1 + calc_linear_form_a2(9) * i2 + 2 * calc_linear_form_a2(10) * i3 + calc_linear_form_a2(11) * i4;
	calc_jacobian_mat(2, 3) = calc_linear_form_a2(3) + calc_linear_form_a2(7) * i1 + calc_linear_form_a2(11) * i3 + 2 * calc_linear_form_a2(12) * i4;
	// Row 3: db_3/di_1, db_3/di_2, db_3/di_3, db_3/di_4
	calc_jacobian_mat(3, 0) = calc_linear_form_b3(0) + 2 * calc_linear_form_b3(4) * i1 + calc_linear_form_b3(5) * i2 + calc_linear_form_b3(6) * i3 + calc_linear_form_b3(7) * i4;
	calc_jacobian_mat(3, 1) = calc_linear_form_b3(1) + calc_linear_form_b3(5) * i1 + 2 * calc_linear_form_b3(8) * i2 + calc_linear_form_b3(9) * i3;
	calc_jacobian_mat(3, 2) = calc_linear_form_b3(2) + calc_linear_form_b3(6) * i1 + calc_linear_form_b3(9) * i2 + 2 * calc_linear_form_b3(10) * i3 + calc_linear_form_b3(11) * i4;
	calc_jacobian_mat(3, 3) = calc_linear_form_b3(3) + calc_linear_form_b3(7) * i1 + calc_linear_form_b3(11) * i3 + 2 * calc_linear_form_b3(12) * i4;

	return 0;
}

//-----------------------------------------------------
// Get the jacobian matrix
int Sextupole::get_jacobian_matrix(Eigen::Matrix<double, 4, 4>& jacobian_mat) {

	get_current_corr(calc_curr_corr);
	jacobian_matrix(calc_curr_corr, true);
	jacobian_mat = calc_jacobian_mat;
	return 0;
}

//-----------------------------------------------------
// Non-linear solver for strengths (Newton's method)
// Enforce a zero contribution of the corrector to b3
int Sextupole::solve_newton(double a1, double b1, double a2, double b3) {

	// Set the target multipole vector
	solve_linear(a1, b1, a2, b3);
	// Initialize the currents
	calc_curr_corr_mat << calc_curr_corr[0], calc_curr_corr[1], calc_curr_corr[2], calc_curr_corr[3];
	// Build the target matrix
	calc_an_bn_target << a1, b1, a2, b3;

	// Get the solver parameters
	int solver_max_iter = get_solver_max_iterations();
	double solver_tolerance = get_solver_tolerance();

	// Iterate
	for (calc_iter = 0; calc_iter < solver_max_iter; calc_iter++) {
		// Compute multipoles
		multipoles_from_currents(calc_curr_main, calc_curr_corr);
		extract_an_bn(calc_an_bn);	// Result in cal_an_bn
		// Compute the jacobian matrix
		jacobian_matrix(calc_curr_corr, false); // Result in calc_jacobian_mat
		// New value of the corrector currents
		calc_curr_corr_mat -= calc_jacobian_mat.inverse() * (calc_an_bn - calc_an_bn_target);
		calc_curr_corr = { calc_curr_corr_mat(0), calc_curr_corr_mat(1), calc_curr_corr_mat(2), calc_curr_corr_mat(3) };
		// Convergence test
		if ((calc_an_bn_target - calc_an_bn).norm() < solver_tolerance) break;
	}

	if (calc_iter == solver_max_iter) return -1;
	else return 0;
}

//-----------------------------------------------------
// Read the values of a1, b1, a2 and b3 and put it in a vector
int Sextupole::extract_an_bn(Eigen::Matrix<double, 4, 1>& an_bn) {

	an_bn(0) = calc_an[0];
	an_bn(1) = calc_bn[0];
	an_bn(2) = calc_an[1];
	an_bn(3) = calc_bn[2];

	return 0;

}

//-----------------------------------------------------
// Get the currents computed with the solver
int Sextupole::get_solver_results(double& curr_main, std::vector<double>& curr_corr) {

	curr_main = calc_curr_main;
	curr_corr = calc_curr_corr;
	return 0;
}

//-----------------------------------------------------
// Get the number of iterations before convergence 
int Sextupole::get_iterations() {
	return calc_iter;
}
//-----------------------------------------------------
// Set the multipoles and the currents from the specified multipoles
int Sextupole::set_currents_from_multipoles(Multipole& multi) {

	// Extract the useful multipole values
	multi.get_an_bn(calc_an, calc_bn);
	double a_1 = calc_an[0];
	double b_1 = calc_bn[0];
	double a_2 = calc_an[1];
	double b_3 = calc_bn[2];
	
	// Compute and set the currents
	switch (solver) {
	case 0: // Linear solver
		if (solve_linear(a_1, b_1, a_2, b_3)) return -1;
		break;
	case 1: // Newton's method
		if (solve_newton(a_1, b_1, a_2, b_3)) return -1;
	}
	
	// Set the currents
	set_current_main(calc_curr_main);
	set_current_corr(calc_curr_corr);

	// Set the multipoles
	set_multipoles(multi);

	return 0;

}

//-----------------------------------------------------
// Set the solver type
// 0: linear
// 1: Newton's method
// Constrained minimization to be implemented
int Sextupole::set_solver(unsigned char s) {
	solver = s;
	return 0;
}

//-----------------------------------------------------
// Get the solver type
unsigned char Sextupole::get_solver_results() {
	return solver;
}

bool Sextupole::has_main_current() {
  return true;
}

void Sextupole::get_strength_names(std::vector<std::string>& names) {
  // The following names are used to create Tango attributes
  names.clear();
  names.push_back("Strength");
  names.push_back("Strength_H");
  names.push_back("Strength_V");
  names.push_back("Strength_SQ");
}

void Sextupole::get_strength_units(std::vector<std::string>& units) {
  // The following units are used to create Tango attributes properties
  units.clear();
  units.push_back("m-2");
  units.push_back("rad");
  units.push_back("rad");
  units.push_back("m-1");
}

//-----------------------------------------------------
// Compute strength(s) from current(s) in standard unit
void Sextupole::compute_strengths(double magnet_rigidity_inv,std::vector<double>& in_currents,std::vector<double>& out_strengths) {

  if (in_currents.size() != 5)
    throw std::invalid_argument("Sextupole::compute_strengths() 5 currents expected");

  std::vector<double> corr_currents;
  for (int i = 0;i < 4;i++)
    corr_currents.push_back(in_currents[i + 1]);

  set_current_main(in_currents[0]);
  set_current_corr(corr_currents);
  set_multipoles_from_currents(in_currents[0],corr_currents);
  Multipole mp = get_multipoles();
  std::vector<double> an = mp.get_an(); // a0[Tmm] a1[T] a2[T/mm]
  std::vector<double> bn = mp.get_bn(); // b0[Tmm] b1[T] b2[T/mm]
  out_strengths.resize(4);
  out_strengths[0] = -(bn[2] * (magnet_rigidity_inv / 1e-3));
  out_strengths[1] = bn[0] * (magnet_rigidity_inv / 1e3);
  out_strengths[2] = -(an[0] * (magnet_rigidity_inv / 1e3));
  out_strengths[3] = -(an[1] * magnet_rigidity_inv);

}

//-----------------------------------------------------
// Compute current(s) from strength(s)
void Sextupole::compute_currents(double magnet_rigidity,std::vector<double>& in_strengths,std::vector<double>& out_currents) {

  if (in_strengths.size() != 4)
    throw std::invalid_argument("Sextupole::compute_currents() 4 strengths expected");

  double b3 = -(in_strengths[0] * (magnet_rigidity * 1e-3));
  double b1 = in_strengths[1] * (magnet_rigidity * 1e3);
  double a1 = -(in_strengths[2] * (magnet_rigidity * 1e3));
  double a2 = -(in_strengths[3] * magnet_rigidity);
  std::vector<double> an = {a1,a2,0};
  std::vector<double> bn = {b1,0,b3};
  Multipole mp = get_multipoles();
  mp.set_an_bn(an, bn);
  set_solver(1); // Newton's method
  set_currents_from_multipoles(mp);

  double curr_main;
  std::vector<double> curr_corr = { 0, 0, 0, 0 };
  get_solver_results(curr_main, curr_corr);
  out_currents.resize(5);
  out_currents[0] = curr_main;
  out_currents[1] = curr_corr[0];
  out_currents[2] = curr_corr[1];
  out_currents[3] = curr_corr[2];
  out_currents[4] = curr_corr[3];

}

} // end namespace MagnetModel