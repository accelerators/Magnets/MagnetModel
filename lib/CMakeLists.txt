cmake_minimum_required(VERSION 2.8)

project(magnet_model)

set(CMAKE_CXX_FLAGS "-g -O2")
set(INCL_USER /segfs/tango/contrib/eigen/include/eigen3)
include_directories(${INCL_USER})
add_library(MagnetModel SHARED SH5Magnet.cpp SH3Magnet.cpp DipoleQuadrupole.cpp Quadrupole.cpp Octupole.cpp Sextupole.cpp Magnet.cpp Multipole.cpp SextuCorrModel.cpp Interpolation.cpp)

set_target_properties(MagnetModel PROPERTIES VERSION 2.0.0 SOVERSION 1)


install(TARGETS MagnetModel DESTINATION lib)
install(FILES SH5Magnet.h SH3Magnet.h DipoleQuadrupole.h Octupole.h Quadrupole.h Sextupole.h Magnet.h Multipole.h SextuCorrModel.h Interpolation.h DESTINATION include)
