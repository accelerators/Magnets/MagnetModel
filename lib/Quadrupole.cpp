#include "Quadrupole.h"

namespace MagnetModel {

//-----------------------------------------------------
// Init a quadrupole
int Quadrupole::init(bool focusing,double scale,std::string strength_file_name, std::string param_file_name, std::string mag_s_n) {

  // Magnet type
  set_magnet_type("quadrupole");
  this->focusing = focusing;

  // Initialize scales, offsets, etc.
  set_params_from_file(param_file_name, mag_s_n);

  // Initialize the excitation curve
  set_scale(get_scale()*scale);
  set_excitation_from_file(strength_file_name);

  return 0;

}

bool Quadrupole::has_main_current() {
  return true;
}

void Quadrupole::get_strength_names(std::vector<std::string>& names) {
  // The following names are used to create Tango attributes
  names.clear();
  names.push_back("Strength");
}

void Quadrupole::get_strength_units(std::vector<std::string>& units) {
  // The following units are used to create Tango attributes properties
  units.clear();
  units.push_back("m-1");
}

//-----------------------------------------------------
// Compute strength(s) from current(s) in standard unit
void Quadrupole::compute_strengths(double magnet_rigidity_inv,std::vector<double>& in_currents,std::vector<double>& out_strengths) {

  if (in_currents.size() != 1)
    throw std::invalid_argument(get_magnet_type()+"::compute_strengths() 1 currents expected");

  out_strengths.resize(1);
  if(focusing) {
    out_strengths[0] = get_excitation_linear(in_currents[0]) * magnet_rigidity_inv;
  } else {
    out_strengths[0] = -get_excitation_linear(in_currents[0]) * magnet_rigidity_inv;
  }

}

//-----------------------------------------------------
// Compute current(s) from strength(s)
void Quadrupole::compute_currents(double magnet_rigidity,std::vector<double>& in_strengths,std::vector<double>& out_currents) {

  if (in_strengths.size() != 1)
    throw std::invalid_argument(get_magnet_type()+"::compute_currents() 1 strengths expected");

  out_currents.resize(1);
  if(focusing) {
    out_currents[0] = solve_main_strength_linear(in_strengths[0] * magnet_rigidity);
  } else {
    out_currents[0] = solve_main_strength_linear(-in_strengths[0] * magnet_rigidity);
  }

}

} // end namespace MagnetModel
